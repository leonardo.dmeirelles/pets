import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import { Routers } from './Routers';
import { store } from './redux/store';
import { GlobalStyles } from './styles/global-styles';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
    <>
        <Provider store={store}>
            <Routers />
            <GlobalStyles />
        </Provider>
    </>,
    document.getElementById('root')
);
