import styled from 'styled-components';

export function Footer() {
    return <Container>Todos direitos reservados</Container>;
}

const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100px;
    background: var(--white);
    z-index: 1;
`;
