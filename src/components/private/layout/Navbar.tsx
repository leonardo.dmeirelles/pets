import styled from 'styled-components';
import { Link, useNavigate } from 'react-router-dom';
import { FaBars } from 'react-icons/fa';
import { Nav, NavItem, NavLink } from 'reactstrap';
import Logo from '../../../images/logo.png';
import { useAppDispatch } from '../../../redux/hooks';
import { toggleNav } from '../../../redux/slices/open-hidden-bar';
import { HiddenBar } from './Hidden-bar';
import { logoutAuth } from '../../../redux/slices/authentication';

export function Navbar() {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const handleClickLogout = () => {
        dispatch(logoutAuth());
        navigate('/');
    };

    return (
        <NavContainer>
            <Content>
                <LogoContent>
                    <img src={Logo} alt="Dog Logo" />
                </LogoContent>
                <SNav>
                    <NavItem>
                        <SLink to="/user">
                            <SNavLink>Walks</SNavLink>
                        </SLink>
                    </NavItem>

                    <NavItem>
                        <SLink to="/user">
                            <SNavLink to="about">Profile</SNavLink>
                        </SLink>
                    </NavItem>

                    <NavItem>
                        <SNavLink onClick={() => handleClickLogout()}>Logout</SNavLink>
                    </NavItem>
                </SNav>

                <BarsIcon>
                    <button type="button" onClick={() => dispatch(toggleNav())}>
                        <FaBars />
                    </button>
                </BarsIcon>
            </Content>
            <HiddenBar />
        </NavContainer>
    );
}

const NavContainer = styled.nav`
    display: flex;
    justify-content: center;
    align-items: center;
    background: var(--white);
    height: 100px;
    width: 100%;
    position: fixed;
    top: 0;
    z-index: 10;
`;

const Content = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
    z-index: 1;
    padding: 0 30px;
    width: 100%;
    max-width: 1200px;
`;

const LogoContent = styled.div`
    position: relative;
    top: 0.25rem;
    left: -1rem;

    img {
        width: 8rem;
        height: 8rem;
    }
`;

const BarsIcon = styled.div`
    display: none;

    @media screen and (max-width: 768px) {
        font-size: 2rem;
        display: block;
        position: absolute;
        top: 1.5rem;
        right: 2rem;
        cursor: pointer;
    }

    button {
        background: transparent;
        border: none;
    }
`;

const SNav = styled(Nav)`
    font-size: 1.5rem;

    @media screen and (max-width: 768px) {
        display: none;
    }
`;

const SLink = styled(Link)`
    text-decoration: none;
`;

const SNavLink = styled(NavLink)`
    color: var(--black);
    text-decoration: none;
    margin: 1rem;
    cursor: pointer;

    &:hover {
        color: red;
        filter: brightness(1.2);
    }
`;
