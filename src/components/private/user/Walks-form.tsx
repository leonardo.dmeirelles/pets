import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Button, FormGroup, Input, Label } from 'reactstrap';
import styled from 'styled-components';

export function WalksForm() {
    const formik = useFormik({
        initialValues: {
            pet: '',
            time: '',
            address: '',
        },

        onSubmit: async (values) => {
            console.log('deu certo');
        },
    });

    return (
        <Container>
            <Form>
                <SFormGroup>
                    <Label>Pet</Label>
                    <Input bsSize="lg" type="select">
                        <option>Cao 1</option>
                        <option>Cao 2</option>
                    </Input>
                </SFormGroup>

                <SFormGroup>
                    <Input bsSize="lg" type="radio" name="time1" />
                    <Label>Time</Label>
                </SFormGroup>
                <SFormGroup>
                    <Input bsSize="lg" type="radio" name="time1" />
                    <Label>Time</Label>

                    <Input bsSize="lg" type="radio" name="time1" />
                    <Label>Time</Label>
                </SFormGroup>

                <SFormGroup>
                    <Label>Address</Label>
                    <Input bsSize="lg" />
                </SFormGroup>
                <br />
                <ButtonDiv>
                    <Button>Send Order</Button>
                </ButtonDiv>
            </Form>
        </Container>
    );
}

const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 80%;
    height: 100%;
    padding: 1rem;

    @media screen and (max-width: 768px) {
        height: 100vh;
        width: 100vw;
    }
`;

const Form = styled.form`
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 100%;
    width: 100%;
`;

const SFormGroup = styled(FormGroup)`
    width: 100%;
`;

const RadioDiv = styled.div`
    display: flex;
`;

const ButtonDiv = styled.div`
    display: flex;
    justify-content: center;
`;
