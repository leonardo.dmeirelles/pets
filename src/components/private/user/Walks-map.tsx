import { Card, CardBody } from 'reactstrap';
import styled from 'styled-components';

export function WalksMap() {
    return (
        <Container>
            <Card>
                <CardBody>Mapa</CardBody>
            </Card>
        </Container>
    );
}

const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 80%;
    height: 100%;
    padding: 1rem;

    @media screen and (max-width: 768px) {
        height: 100vh;
        width: 100%;
    }
`;
