import styled from 'styled-components';
import Video from '../../../images/video.mp4';

export function VideoComponent() {
    return (
        <Container>
            <Content>
                <VideoContent autoPlay loop muted src={Video} />
            </Content>
        </Container>
    );
}

const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    position: relative;
    z-index: -10;
`;

const Content = styled.div`
    position: absolute;
    width: 100%;
    height: 100%;
    overflow: hidden;
`;

const VideoContent = styled.video`
    width: 100%;
    height: 100%;
    -o-object-fit: cover;
    object-fit: cover;
`;
