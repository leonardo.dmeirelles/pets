import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { FaBars } from 'react-icons/fa';
import { Button, Nav, NavItem, NavLink } from 'reactstrap';
import Logo from '../../../images/logo.png';
import { useAppDispatch } from '../../../redux/hooks';
import { toggleNav } from '../../../redux/slices/open-hidden-bar';
import { toggleModal } from '../../../redux/slices/open-modal';

export function Navbar() {
    const dispatch = useAppDispatch();

    return (
        <NavContainer>
            <Content>
                <LogoContent>
                    <img src={Logo} alt="Dog Logo" />
                </LogoContent>
                <SNav>
                    <NavItem>
                        <SNavLink to="home" onClick={() => dispatch(toggleModal('home'))}>
                            Home
                        </SNavLink>
                    </NavItem>

                    <NavItem>
                        <SNavLink to="about" onClick={() => dispatch(toggleModal('about'))}>
                            About
                        </SNavLink>
                    </NavItem>
                </SNav>

                <LoginButton>
                    <Link to="/register">
                        <Button outline>Register </Button>
                    </Link>

                    <Link to="/login">
                        <Button outline>Login </Button>
                    </Link>
                </LoginButton>

                <BarsIcon>
                    <button type="button" onClick={() => dispatch(toggleNav())}>
                        <FaBars />
                    </button>
                </BarsIcon>
            </Content>
        </NavContainer>
    );
}

const NavContainer = styled.nav`
    display: flex;
    justify-content: center;
    align-items: center;
    background: var(--white);
    height: 100px;
    width: 100vw;
    position: sticky;
    top: 0;
    z-index: 10;
`;

const Content = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
    z-index: 1;
    padding: 0 30px;
    width: 100%;
    max-width: 1200px;
`;

const LogoContent = styled.div`
    position: relative;
    top: 0.25rem;
    left: -1rem;

    img {
        width: 8rem;
        height: 8rem;
    }
`;

const BarsIcon = styled.div`
    display: none;

    @media screen and (max-width: 768px) {
        font-size: 2rem;
        display: block;
        position: absolute;
        top: 1.5rem;
        right: 2rem;
        cursor: pointer;
    }

    button {
        background: transparent;
        border: none;
    }
`;

const SNav = styled(Nav)`
    font-size: 1.5rem;

    @media screen and (max-width: 768px) {
        display: none;
    }
`;

const SNavLink = styled(NavLink)`
    color: var(--black);
    text-decoration: none;
    margin: 1rem;
    cursor: pointer;

    /* &.active {
        border-bottom: 4px solid #01bf71;
    } */

    &:hover {
        color: red;
        filter: brightness(1.2);
    }
`;

const LoginButton = styled.div`
    display: flex;
    Button {
        margin: 0 0.5rem;
    }

    @media screen and (max-width: 768px) {
        display: none;
    }
`;
