import styled from 'styled-components';
import { Footer } from './Footer';
import { Navbar } from './Navbar';
import { HiddenBar } from './Hidden-bar';

export function Layout({ children }: any) {
    return (
        <Container>
            <Navbar />
            <HiddenBar />
            <Main>{children}</Main>
            <Footer />
        </Container>
    );
}

const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100vh;
`;

const Main = styled.div`
    flex: 1;
`;
