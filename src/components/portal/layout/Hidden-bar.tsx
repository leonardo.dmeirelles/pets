import { Link } from 'react-scroll';
import { Card, Collapse, Nav, NavItem } from 'reactstrap';
import styled from 'styled-components';
import { useAppSelector } from '../../../redux/hooks';

export function HiddenBar() {
    const open = useAppSelector((state) => state.openBar.open);

    return (
        <SCollapse isOpen={open}>
            <Card>
                <Nav justified vertical>
                    <br />
                    <NavItem>
                        <SLink to="home">Home</SLink>
                    </NavItem>
                    <br />
                    <NavItem>
                        <SLink to="home">About</SLink>
                    </NavItem>
                    <br />
                    <NavItem>
                        <SLink to="home">Register</SLink>
                    </NavItem>
                    <br />
                    <NavItem>
                        <SLink to="home">Login</SLink>
                    </NavItem>
                    <br />
                </Nav>
            </Card>
        </SCollapse>
    );
}

const SCollapse = styled(Collapse)`
    position: absolute;
    top: 100px;
    width: 100%;
    z-index: 999;
`;

const SLink = styled(Link)`
    color: var(--black);
    text-decoration: none;
    cursor: pointer;
    font-size: 1.5rem;

    &:hover {
        color: var(--purple);
        filter: brightness(1.2);
    }
`;
