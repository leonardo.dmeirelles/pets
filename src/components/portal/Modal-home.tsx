import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { toggleModal } from '../../redux/slices/open-modal';

export function ModalHome() {
    const dispatch = useAppDispatch();

    const open = useAppSelector((state) => state.openModal.open);
    const type = useAppSelector((state) => state.openModal.type);

    return (
        <Modal centered size="lg" isOpen={type === 'home' ? open : false} toggle={() => dispatch(toggleModal('home'))}>
            <ModalHeader charCode="Y" toggle={() => dispatch(toggleModal('home'))}>
                Home
            </ModalHeader>
            <ModalBody>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                deserunt mollit anim id est laborum. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Adipisci
                iure numquam labore mollitia iste voluptas rem a fugiat eum vero. Architecto, pariatur quidem. Qui
                quaerat ea harum in eaque eos veniam eius ad eligendi sit aliquid officia ab voluptate dolor nisi,
                magnam cupiditate quasi animi. Explicabo, nihil. Magnam dolores natus voluptatum vel eius voluptas
                quidem eveniet debitis? Excepturi quisquam, laborum mollitia est dolor ipsam magni odio officia
                veritatis architecto atque, deleniti numquam eaque molestias id illo veniam temporibus voluptate iste
                culpa voluptatibus aliquam. Saepe culpa soluta omnis veniam quidem maxime molestias! Nemo reiciendis,
                voluptatem totam accusantium deserunt provident nostrum saepe dicta suscipit doloremque architecto,
                incidunt laboriosam nobis quibusdam in amet unde ex, repudiandae a? Molestiae cupiditate voluptatum
                laudantium architecto veritatis expedita placeat iste. Reprehenderit sit architecto laudantium
                consequuntur eligendi eveniet, deleniti placeat assumenda delectus, doloremque quasi illo. Iure magnam
                fugiat possimus quas tenetur, quaerat dolorem architecto distinctio. Exercitationem officiis tenetur
                eveniet earum recusandae pariatur suscipit quae voluptatem reprehenderit dolorem reiciendis
                voluptatibus, in inventore. Praesentium inventore ipsam autem et dolore ea laudantium nemo vero at ut,
            </ModalBody>
        </Modal>
    );
}
