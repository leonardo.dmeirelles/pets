import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { toggleModal } from '../../redux/slices/open-modal';

export function ModalAbout() {
    const dispatch = useAppDispatch();

    const open = useAppSelector((state) => state.openModal.open);
    const type = useAppSelector((state) => state.openModal.type);

    return (
        <Modal
            centered
            size="lg"
            isOpen={type === 'about' ? open : false}
            toggle={() => dispatch(toggleModal('about'))}
        >
            <ModalHeader charCode="Y" toggle={() => dispatch(toggleModal('about'))}>
                About
            </ModalHeader>
            <ModalBody>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                deserunt mollit anim id est laborum.
            </ModalBody>
        </Modal>
    );
}
