import { http } from '../config/http';

interface AuthLoginService {
    data: {
        token: string;
    };
}

export const authenticationLoginService = async (data: any): Promise<AuthLoginService> => http.post('/login', data);

export const authenticationRegisterService = async (data: any): Promise<AuthLoginService> =>
    http.post('/register', data);
