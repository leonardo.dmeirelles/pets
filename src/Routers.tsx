import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import { Portal } from './views/portal/index';
import { Private } from './views/private/index';
import { Login } from './views/portal/Login';
import { isAuthenticated } from './config/storage';

function PrivateRoute({ children, path }: { children: JSX.Element; path: string }) {
    const isLoggedIn = path === '/login';

    if (!isAuthenticated() && !isLoggedIn) {
        return <Navigate to="/login" />;
    }

    if (isAuthenticated() && isLoggedIn) {
        return <Navigate to="/user/" />;
    }

    return children;
}

export function Routers() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/*" element={<Portal />} />

                <Route
                    path="/login"
                    element={
                        <PrivateRoute path="/login">
                            <Login />
                        </PrivateRoute>
                    }
                />

                <Route
                    path="/user/*"
                    element={
                        <PrivateRoute path="/user">
                            <Private />
                        </PrivateRoute>
                    }
                />
            </Routes>
        </BrowserRouter>
    );
}
