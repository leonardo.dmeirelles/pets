import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import openHiddenBarSlice from './slices/open-hidden-bar';
import openModalSlice from './slices/open-modal';
import authenticationSlice from './slices/authentication';

export const store = configureStore({
    reducer: {
        openBar: openHiddenBarSlice,
        openModal: openModalSlice,
        authentication: authenticationSlice,
    },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
