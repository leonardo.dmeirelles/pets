import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface InitialState {
    type: string;
    open: boolean;
}

const initialState: InitialState = {
    type: '',
    open: false,
};

export const openModalSlice = createSlice({
    name: 'open',
    initialState,
    reducers: {
        toggleModal: (state, action: PayloadAction<string>) => {
            state.open = !state.open;
            state.type = action.payload;
        },
    },
});

export const { toggleModal } = openModalSlice.actions;

export default openModalSlice.reducer;
