import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit';
import jwt, { JwtPayload } from 'jsonwebtoken';
import { http } from '../../config/http';
import { getTokenLocalStorage, removeTokenLocalStorage, saveTokenLocalStorage } from '../../config/storage';
import { authenticationLoginService } from '../../services/authenticationService';

export const loginAuth: any = createAsyncThunk('login', async (data, { dispatch }) => {
    dispatch(authLoading(true));

    const responseToken = await authenticationLoginService(data);

    saveTokenLocalStorage(responseToken.data.token);

    http.defaults.headers['Authorization'] = `bearer ${responseToken.data.token}`;

    return responseToken.data.token;
});

interface InitialState {
    token: string;
    user:
        | {
              _id: string;
              name: string;
              email: string;
              phone: string;
              iat: number;
              exp: number;
          }
        | JwtPayload;
    type: string;
    loading: boolean;
}

const initialState: InitialState = {
    token: getTokenLocalStorage() || '',
    user: (jwt.decode(getTokenLocalStorage() || '') as JwtPayload) || {},
    type: '',
    loading: false,
};

export const authenticationSlice = createSlice({
    name: 'authentication',
    initialState,
    reducers: {
        authLoading: (state, action: PayloadAction<boolean>) => {
            state.loading = action.payload;
        },

        logoutAuth: (state: any) => {
            removeTokenLocalStorage();
            state.token = '';
            state.user = {};
            state.type = '';
        },
    },
    extraReducers: {
        [loginAuth.fulfilled]: (state: any, action: PayloadAction<any>) => {
            state.token = action.payload;
            state.user = jwt.decode(action.payload);
            state.type = 1;
            state.loading = false;
        },

        [loginAuth.rejected]: (state: any, action: PayloadAction) => {
            state.loading = false;
        },
    },
});

export const { authLoading, logoutAuth } = authenticationSlice.actions;

export default authenticationSlice.reducer;
