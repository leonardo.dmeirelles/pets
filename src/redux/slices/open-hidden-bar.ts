import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    open: false,
};

export const openBarSlice = createSlice({
    name: 'open',
    initialState,
    reducers: {
        toggleNav: (state) => {
            state.open = !state.open;
        },
    },
});

export const { toggleNav } = openBarSlice.actions;

export default openBarSlice.reducer;
