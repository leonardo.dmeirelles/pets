import { Route, Routes } from 'react-router-dom';
import { Layout } from '../../components/private/layout';
import { PetsWalk } from './Pets-walk';

export function Private() {
    return (
        <Layout>
            <Routes>
                <Route path="/" element={<PetsWalk />} />
            </Routes>
        </Layout>
    );
}
