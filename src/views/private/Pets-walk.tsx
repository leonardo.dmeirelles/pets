import styled from 'styled-components';
import { WalksMap } from '../../components/private/user/Walks-map';
import { WalksForm } from '../../components/private/user/Walks-form';

export function PetsWalk() {
    return (
        <Container>
            <LeftContent>
                <WalksForm />
            </LeftContent>

            <RightContent>
                <WalksMap />
            </RightContent>
        </Container>
    );
}

const Container = styled.div`
    display: flex;
    justify-content: space-around;
    align-items: center;
    height: 100%;
    width: 100%;

    @media screen and (max-width: 768px) {
        flex-direction: column;
    }
`;

const LeftContent = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    height: 50%;
    width: 60%;
`;

const RightContent = styled.div`
    height: 50%;
    width: 40%;
`;
