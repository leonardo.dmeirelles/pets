import { Route, Routes } from 'react-router-dom';
import { HomePortal } from './HomePortal';
import { Layout } from '../../components/portal/layout';

export function Portal() {
    return (
        <Layout>
            <Routes>
                <Route path="/" element={<HomePortal />} />
            </Routes>
        </Layout>
    );
}
