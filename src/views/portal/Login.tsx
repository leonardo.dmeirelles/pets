import { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Button, Card, CardBody, CardText, CardTitle, FormGroup, Input, Label } from 'reactstrap';
import styled from 'styled-components';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { loginAuth } from '../../redux/slices/authentication';

export function Login() {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const token = useAppSelector((state) => state.authentication.token);

    useEffect(() => {
        if (token) {
            navigate('/user');
        }
    }, [token]);

    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
        },

        validationSchema: Yup.object().shape({
            email: Yup.string().required('Fill in an email !').email('Fill in a valid email format !'),
            password: Yup.string()
                .required('Fill in an password !')
                .min(6, 'Password must have at least 6 characters !'),
        }),

        onSubmit: async (values) => {
            dispatch(loginAuth(values));
        },
    });

    return (
        <Container>
            <SCard>
                <Form onSubmit={formik.handleSubmit}>
                    <SCardBody>
                        <CardTitle tag="h5">Login</CardTitle>

                        <SCardText>
                            <FormGroup>
                                <Label>Email</Label>
                                <SInput
                                    bsSize="lg"
                                    id="email"
                                    name="email"
                                    value={formik.values.email}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    invalid={formik.touched.email && formik.errors.email ? true : null}
                                />
                                <br />
                                {formik.touched.email && formik.errors.email ? <h5>{formik.errors.email}</h5> : null}
                            </FormGroup>

                            <FormGroup>
                                <Label>Password</Label>
                                <SInput
                                    bsSize="lg"
                                    id="password"
                                    name="password"
                                    value={formik.values.password}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    invalid={formik.touched.password && formik.errors.password ? true : null}
                                />
                                <br />
                                {formik.touched.password && formik.errors.password ? (
                                    <h5>{formik.errors.password}</h5>
                                ) : null}
                            </FormGroup>
                        </SCardText>

                        <ButtonContent>
                            <Link to="/">
                                <Button outline color="secondary">
                                    Back to Home
                                </Button>
                            </Link>

                            <Button type="submit" outline color="secondary">
                                Login
                            </Button>
                        </ButtonContent>
                    </SCardBody>
                </Form>
            </SCard>
        </Container>
    );
}

const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100vh;
    width: 100%;
`;

const SCard = styled(Card)`
    height: 40%;
    width: 600px;
    margin: 6rem;
    border-radius: 8px;

    @media screen and (max-width: 768px) {
        height: 70%;
        margin: 1rem;
    }
`;

const Form = styled.form`
    display: flex;
    height: 100%;
`;

const SCardBody = styled(CardBody)`
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
`;

const SInput = styled(Input)`
    width: 100%;
`;

const SCardText = styled(CardText)`
    display: flex;
    flex-direction: column;
    justify-content: center;
    width: 80%;

    @media screen and (max-width: 768px) {
        width: 100%;
    }
`;

const ButtonContent = styled.div`
    display: flex;
    justify-content: space-around;
    width: 100%;
`;
