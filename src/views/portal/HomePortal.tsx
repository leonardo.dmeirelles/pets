import styled from 'styled-components';
import { VideoComponent } from '../../components/portal/layout/Video-component';
import { ModalAbout } from '../../components/portal/Modal-about';
import { ModalHome } from '../../components/portal/Modal-home';

export function HomePortal() {
    return (
        <Container>
            <VideoComponent />

            <div>
                <ModalHome />

                <ModalAbout />
            </div>
        </Container>
    );
}
const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
`;
