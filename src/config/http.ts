import axios from 'axios';

const urlDefault = 'http://localhost:5000';

export const http: any = axios.create({
    baseURL: urlDefault,
});

http.defaults.headers['Content-Type'] = 'application/json';

// if (getTokenLocalStorage()) {
//     http.defaults.headers['Authorization'] = `bearer ${getTokenLocalStorage()}`
// };

// http.interceptors.response.use(
//     (response) => response,

//     (error) => {
//         switch (error.response.status) {
//             case 401:
//                 store.dispatch(logoutAuthentication() as any);
//                 return Promise.reject(error);

//             default:
//                 return Promise.reject(error);
//         }
//     }
// );
